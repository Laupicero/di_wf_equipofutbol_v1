﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    /// <summary>
    /// Clase principal y única del formulario
    /// que nos ayudará con la lógica del programa
    /// </summary>
    public partial class Form1 : Form
    {
        // Variable contante que nos servirá para establecer el máximo de jugadores por cada equipo
        private const int numMaximoJugadoresPorEquipo = 7;

        //Ambas variables nos llevará el recuento del número de jugadores que tiene cada equipo 
        private int numJugadoresEquipoA;
        private int numJugadoresEquipoB;

        //Aquí guardaremos los datos/String de los jugadores/árbitros
        private String[] jugadores;
        private String[] arbitros;

        //Booleano que nops hará diferente funcionalidad si hemos decidido borrar el listado con los jugadores principales
        private Boolean borrarJugadoresPrincipales;


        //---------------------
        // Constructor
        //---------------------
        /// <summary>
        /// Daremos el valor a nuestros Arrys con los datos de los jugadores
        /// Y rellenaremos con estos los ListBox y Combobox
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            this.numJugadoresEquipoA = 0;
            this.numJugadoresEquipoB = 0;
            this.borrarJugadoresPrincipales = false;

            this.jugadores = new String[]{"Casillas", "Bufon", "Pelé", "Maradona", "Messi", "Cristiano-Ronaldo",
                "Alfredo-di-Stéfano", "Beckenbauer", "Ronaldo-Nazario", "Juanito", "Zinedine-Zidane", "Johan-Cruyff",
                "Dani-Güiza", "Ronaldinho", "Gerd-Muller", "Marco-van-Basten", "Xavi-Hernández", "Raul-Gonzalez" };

            this.arbitros = new String[] {"Iturralde","Lopez-Nieto","Guruzeta","Ramos-Marcos", "Pierluigi-Collina","El-tio-de-negro" };

            //rellenarListadoJugadores(this.lbTodosJugadores);
            rellenarComboBoxArbitraje();

            //Configuramos los DataTimePicker
            //Con la fecha
            dtpSeleccioFechaPartido.MaxDate = new DateTime(2025, 12, 31);

            //Con la hora
            dtpHoraPartido.CustomFormat = "HH:mm";
            dtpHoraPartido.ShowUpDown = true;

            //Establecemos los valores por defecto en ambos DataTimePicker
            this.lbDiaPartido.Text = dtpSeleccioFechaPartido.Value.ToShortDateString();
            this.lbHoraPartido.Text = dtpHoraPartido.Value.ToShortTimeString();
        }

        //=====================================
        //      MÉTODOS DEL PROGRAMA
        //=====================================

        //----------------------------------------------------
        // Método de los botones principales del formulario
        // (Estos están justo debajo del listBox con el nombre de todos los jugadores)
        //----------------------------------------------------


        //Botón que nos cargará el listado de jugadores a través de un fichero que seleccionemos
        // si el fichero nos diera un error o no es el indicado, nos lo rellenará con el array que teníamos desde el principio
        private void btnCargarJugad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            String jugadoLinea;

            ofd.Filter = "Archivos de texto(*.txt) | *.txt";
            ofd.Title = "Abrir Archivo";

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                StreamReader flujo = new StreamReader(ofd.FileName);
                do
                {
                    jugadoLinea = flujo.ReadLine();
                    if (jugadoLinea != null)
                        lbTodosJugadores.Items.Add(jugadoLinea.Trim());
                }
                while (jugadoLinea != null);
            }            
            //rellenarListadoJugadores(this.lbTodosJugadores);
        }

        /// <summary>
        /// Botón que nos eliminará a todos los jugadores de la lista principal
        /// En el caso de pulsarlo la variableGlobal 'borrarJugadoresPrincipales' pasará a 'true'.
        /// 
        /// Esto hará que ignore el método 'comprobarJugadorRepetido()' para comprobarnos que no se han insertado
        /// los mismos jugadores que tendremos en el Array
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEliminarJug_Click(object sender, EventArgs e)
        {
            this.lbTodosJugadores.Items.Clear();
            this.borrarJugadoresPrincipales = true;
        }


        /// <summary>
        /// Botón que nos añadirá jugadores a nuestra lista prinicipal (ListBox)
        /// Nos realizará tamnién una comprobación en la que nos comprobará si el jugador que queremos introducir
        /// existe ya en el array que hemos usado previamente para cargar el listbox.
        /// 
        /// Si hemos decidido Borrar el listBoxPrincipal con todos los jugadores y queremos 
        /// añadir los mismos nombres de los jugadores anteriores (del array jugadores)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnnadirJugador_Click(object sender, EventArgs e)
        {
            //comprobamos que haya texto escrito
            if (tbAnnadirJugador.TextLength > 0)
            {
                //Comprobamos que no hemos borrado previamente el listado
                if (!borrarJugadoresPrincipales)
                {
                    //Si esta completo el ListBox comprobamos si hay algún jugador repetido
                    if (!comprobarJugadorRepetido(tbAnnadirJugador.Text, "listadoPrincipal"))
                    {
                        this.lbTodosJugadores.Items.Add(tbAnnadirJugador.Text);
                        tbAnnadirJugador.Clear();
                    }
                    else
                    {
                        MessageBox.Show("¡Inserte el nombre de un jugador que no este en el listado principal!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    this.lbTodosJugadores.Items.Add(tbAnnadirJugador.Text);
                    tbAnnadirJugador.Clear();
                }                    
            }
            else
            {
                MessageBox.Show("¡Escriba primero el nombre de un jugador!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        //----------------------------------------------------
        //----------------------------------------------------
        //  Métodos para insertar jugadores en los equipos
        //----------------------------------------------------
        //----------------------------------------------------

        //Insertar en el equipo'A' uno o varios jugadores
        private void btnInsertJugEquipA_Click(object sender, EventArgs e)
        {
            //Primero comprobamos que no hay más de '7' jugadores
            if(this.numJugadoresEquipoA < numMaximoJugadoresPorEquipo)
            {
                //Comprobamos que haya selecccionado un jugador o varios
                if (this.lbTodosJugadores.SelectedItems.Count >= 1)
                {
                    //Si sólo hay uno seleccionado
                    if (this.lbTodosJugadores.SelectedItems.Count == 1)
                    {
                        string jugadorItem = this.lbTodosJugadores.SelectedItem.ToString();
                        this.lbTodosJugadores.Items.Remove(jugadorItem);
                        //Comprobaremos también si existe ya ese jugador
                        if (!comprobarJugadorRepetido(jugadorItem, "equipoA"))
                        {
                            this.lbEquiA.Items.Add(jugadorItem);
                            this.numJugadoresEquipoA += 1;
                        }
                        else
                        {
                            MessageBox.Show("¡Jugador Repetido!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    // Si hay más de uno seleccionado
                    else
                    {
                        ArrayList jugadoresSeleccionadosList = new ArrayList();
                        foreach (String jugador in this.lbTodosJugadores.SelectedItems)
                        {
                            jugadoresSeleccionadosList.Add(jugador);
                            // No se puede borrar así porque cambia el número de elemetos: this.lbTodosJugadores.Items.Remove(jugador);
                        }

                        annadirJugadores("equipoA", jugadoresSeleccionadosList);
                    }
                }
                else
                {
                    MessageBox.Show("¡Seleccione previamente a algún jugador!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("¡Este equipo está completado!" +
                    "\n¡No se pueden seleccionar más jugadores!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }// Fin insertar en Equipo'A'

        


        //Insertar en el equipo'B' uno o varios jugadores
        private void btnInsertJugEquipB_Click(object sender, EventArgs e)
        {
            if (this.numJugadoresEquipoB < numMaximoJugadoresPorEquipo)
            {
                //Comprobamos que haya selecccionado un jugador o varios
                if (this.lbTodosJugadores.SelectedItems.Count >= 1)
                {
                    //Si sólo hay uno seleccionado
                    if (this.lbTodosJugadores.SelectedItems.Count == 1)
                    {
                        string jugadorItem = this.lbTodosJugadores.SelectedItem.ToString();
                        this.lbTodosJugadores.Items.Remove(jugadorItem);
                        if (!comprobarJugadorRepetido(jugadorItem, "equipoB"))
                        {
                            this.lbEquiB.Items.Add(jugadorItem);
                            this.numJugadoresEquipoB += 1;
                        }
                        else
                        {
                            MessageBox.Show("¡Jugador Repetido!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    // Si hay más de uno seleccionado
                    else
                    {
                        ArrayList jugadoresSeleccionadosList = new ArrayList();
                        foreach (String jugador in this.lbTodosJugadores.SelectedItems)
                        {
                            jugadoresSeleccionadosList.Add(jugador);
                        }

                        annadirJugadores("equipoB", jugadoresSeleccionadosList);
                    }
                }
                else
                {
                    MessageBox.Show("¡Seleccione previamente a algún jugador!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("¡Este equipo está completado!" +
                    "\n¡No se pueden seleccionar más jugadores!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }// Fin insertar en Equipo'B'



        //----------------------------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------------------
        // Métodos para devolver a uno/varios jugadores (que estén previamente seleccionados) del 
        // equipo que hayamos seleccionado previamente
        //----------------------------------------------------------------------------------------------------------------

        //Devolveremos al jugador/jugadores seleccionados del 'equipoA' al listado principal
        private void btnDevolverJugEquipA_Click(object sender, EventArgs e)
        {
            if (this.lbEquiA.SelectedItems.Count >= 1)
            {
                //si sólo se ha seleccionado a un jugador
                if (this.lbEquiA.SelectedItems.Count == 1)
                {
                    string jugadorSelec = this.lbEquiA.SelectedItem.ToString();
                    this.lbEquiA.Items.Remove(jugadorSelec);
                    this.lbTodosJugadores.Items.Add(jugadorSelec);
                    this.numJugadoresEquipoA -= 1;
                }
                //Si se ha seleccionado a varios
                else
                {
                    ArrayList jugadoresSeleccionadosList = new ArrayList();
                    foreach (String jugador in this.lbEquiA.SelectedItems)
                    {
                        jugadoresSeleccionadosList.Add(jugador);
                    }

                    annadirJugadores("listadoPrincipal", jugadoresSeleccionadosList, "equipoA");
                }
            }
            else
            {
                MessageBox.Show("¡Debe seleccionar primero a un jugador!","ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        
        //Devolveremos al jugador/jugadores seleccionados del 'equipoB' al listado principal
        private void btnDevolverJugEquipB_Click(object sender, EventArgs e)
        {
            if (this.lbEquiB.SelectedItems.Count >= 1)
            {
                //si sólo se ha seleccionado a un jugador
                if (this.lbEquiB.SelectedItems.Count == 1)
                {
                    string jugadorSelec = this.lbEquiB.SelectedItem.ToString();
                    this.lbEquiB.Items.Remove(jugadorSelec);
                    this.lbTodosJugadores.Items.Add(jugadorSelec);
                    this.numJugadoresEquipoB -= 1;
                }
                //Si se ha seleccionado a varios
                else
                {
                    ArrayList jugadoresSeleccionadosList = new ArrayList();
                    foreach (String jugador in this.lbEquiB.SelectedItems)
                    {
                        jugadoresSeleccionadosList.Add(jugador);
                    }

                    annadirJugadores("listadoPrincipal", jugadoresSeleccionadosList, "equipoB");
                }
            }
            else
            {
                MessageBox.Show("¡Debe seleccionar primero a un jugador!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }



        //------------------------------------------------------------------------
        //------------------------------------------------------------------------
        //Métodos que nos devuelve todos los jugadores del equipo que hayamos seleccionado
        // a la lista general de jugadores
        //------------------------------------------------------------------------

        //Devolvemos todos los jugadores del 'equipoA' al listado principal
        private void btnDevolverTodosJugEquipA_Click(object sender, EventArgs e)
        {
            ArrayList jugadores = new ArrayList();
            foreach (String jugador in this.lbEquiA.Items)
            {
                jugadores.Add(jugador);
                this.lbTodosJugadores.Items.Add(jugador);
                this.numJugadoresEquipoA -= 1;
            }
            borrarJugadoresListado("equipoA", jugadores);
        }

        //Devolvemos todos los jugadores del 'equipoB' al listado principal
        private void btnDevolverTodosJugEquipB_Click(object sender, EventArgs e)
        {
            ArrayList jugadores = new ArrayList();
            foreach (String jugador in this.lbEquiB.Items)
            {
                jugadores.Add(jugador);
                this.lbTodosJugadores.Items.Add(jugador);
                this.numJugadoresEquipoB -= 1;
            }
            borrarJugadoresListado("equipoB", jugadores);
        }


        //------------------------------------------------------------------------
        //------------------------------------------------------------------------
        // Métodos de los comboBox de los arbitros
        //
        // La funcionalidad de estos comboBox será así:
        // Primero deberemos seleccionar en órden a los árbitros, depués Liner-1 y luego Liner-2
        // Observaremos que se irán desactivando poco a poco
        // Podemos tambíen cambiar al árbitro/Liner principal cuando seleccionemos otro de nuevo
        //------------------------------------------------------------------------


        //Seleccionar Árbitro
        /// <summary>
        /// Seleccionamos el árbitro
        /// Primero nos buscará en nuestra label 'xxx' que hará referencia al árbitro
        /// Sino lo encuentra, cambiaremos la posición, en este caso la primera que corresponderá al árbitro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbArbitro_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool cambioArbitro = true;
            // Comprobamos que se haya seleccionado
            if(cbArbitro.SelectedIndex > 0)
            {
                String arbitroItem = cbArbitro.SelectedItem.ToString();
                borrarArbitro(arbitroItem);

                //Formateamos la cadena para quitarles las ',' y así detectar 'xxx'
                String[] contenidoLabel = formatearCadena(lbArbitros.Text, "input");


                //Comprobamos primero que no haya árbitro
                for (int i=0; i < contenidoLabel.Length; i++)
                {
                    if (contenidoLabel[i] == "xxx")
                    {
                        contenidoLabel[i] = arbitroItem;
                        cambioArbitro = false;
                    }                        
                }

                //Para cambiar el árbitro principal
                // Al no detectar en el anterior bucle el patrón 'xxx'
                // Cambiamos el arbitro directamente, añadiendo el que estaba previemente de nuevo en el comboBox y eliminando el anterior
                if (cambioArbitro)
                {
                    insertarArbitro(contenidoLabel[0]);
                    contenidoLabel[0] = arbitroItem;
                    borrarArbitro(arbitroItem);
                }

                //Formateamos la cadena para añadir las ','
                contenidoLabel = formatearCadena(String.Join(" ", contenidoLabel), "output");
                lbArbitros.Text = String.Join("", contenidoLabel);
               
                //Activamos el ComboBox del Liner1
                cbLiner1.Enabled = true;
            }
        }

        

        //Seleccionar Liner1 (Misma ejecución que el de árbitro)
        private void cbLiner1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool cambioLiner1 = true;
            // Comprobamos que se haya seleccionado
            if (cbLiner1.SelectedIndex > 0)
            {
                String arbitroItem = cbLiner1.SelectedItem.ToString();
                borrarArbitro(arbitroItem);

                //Formateamos la cadena para quitarles las ',' y así detectar 'xxx'
                String[] contenidoLabel = formatearCadena(lbArbitros.Text, "input");


                //Comprobamos primero que no haya árbitro
                for (int i = 0; i < contenidoLabel.Length; i++)
                {
                    if (contenidoLabel[i] == "xxxx")
                    {
                        contenidoLabel[i] = arbitroItem;
                        cambioLiner1 = false;
                    }
                }

                if (cambioLiner1)
                {
                    insertarArbitro(contenidoLabel[1]);
                    contenidoLabel[1] = arbitroItem;
                    borrarArbitro(arbitroItem);
                }

                //Formateamos la cadena para añadir las ','
                contenidoLabel = formatearCadena(String.Join(" ", contenidoLabel), "output");
                lbArbitros.Text = String.Join("", contenidoLabel);

                //Activamos el ComboBox del Liner2
                cbLiner2.Enabled = true;
            }
        }


        //Seleccionar Liner2 (Misma ejecución que el de árbitro y Liner1)
        private void cbLiner2_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool cambioLiner2 = true;
            // Comprobamos que se haya seleccionado
            if (cbLiner2.SelectedIndex > 0)
            {
                String arbitroItem = cbLiner2.SelectedItem.ToString();
                borrarArbitro(arbitroItem);

                //Formateamos la cadena para quitarles las ',' y así detectar 'xxx'
                String[] contenidoLabel = formatearCadena(lbArbitros.Text, "input");


                //Comprobamos primero que no haya árbitro
                for (int i = 0; i < contenidoLabel.Length; i++)
                {
                    if (contenidoLabel[i] == "xxxxx")
                    {
                        contenidoLabel[i] = arbitroItem;
                        cambioLiner2 = false;
                    }
                }

                if (cambioLiner2)
                {
                    insertarArbitro(contenidoLabel[2]);
                    contenidoLabel[2] = arbitroItem;
                    borrarArbitro(arbitroItem);
                }

                //Formateamos la cadena para añadir las ','
                contenidoLabel = formatearCadena(String.Join(" ", contenidoLabel), "output");
                lbArbitros.Text = String.Join("", contenidoLabel);

                btnAnnadirLiner2.Enabled = true;
                btnAnnadirLiner1.Enabled = true;
                btnAnnadriArbitro.Enabled = true;
            }
            
        }



        //-----------------------------------------------------------------
        // Métodos de los botones para añadir más de un arbitro / Liner
        //
        // Estos botones no se activarán hasta que no haya insertado a 
        // un árbitro de cada tipo previamente (árbitro, liner-1 y liner-2)
        //-----------------------------------------------------------------
        
        //Añadir Arbitro Extra
        /// <summary>
        /// Nos añadira un árbitro extra, sólo si se ha introducido uno previamente
        /// Sino, aparecerá una ventana (MessageBox) advirtiéndolo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnnadriArbitro_Click(object sender, EventArgs e)
        {
            //Comprobamos antes que no se haya añadido ningún arbitro previamente
            if (arbitroAnnadidoPreviamente("arbitro"))
            {
                lbArbitros.Text += ", xxx";                
            }
            else
            {
                MessageBox.Show("¡Debe seleccionar previamente un arbitro antes de añadir otro!", "¡INFORMACIÓN!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        

        //Añadir Liner1 Extra
        private void btnAnnadirLiner1_Click(object sender, EventArgs e)
        {
            //Comprobamos antes que no se haya añadido ningún arbitro previamente
            if (arbitroAnnadidoPreviamente("liner1"))
            {
                lbArbitros.Text += ", xxxx";
            }
            else
            {
                MessageBox.Show("¡Debe seleccionar previamente al primer Liner antes de añadir otro!", "¡INFORMACIÓN!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        
        //Añadir Liner2 Extra
        private void btnAnnadirLiner2_Click(object sender, EventArgs e)
        {
            //Comprobamos antes que no se haya añadido ningún arbitro previamente
            if (arbitroAnnadidoPreviamente("liner2"))
            {
                lbArbitros.Text += ", xxxxx";
            }
            else
            {
                MessageBox.Show("¡Debe seleccionar previamente a uno de los del segundo Liner antes de añadir otro!", "¡INFORMACIÓN!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        //---------------------------------------------
        // Métodos de los DatatimePicker
        //---------------------------------------------

        //Fecha (Qué sólo se puedan jugar Martes y Jueves)
        private void dtpSeleccioFechaPartido_ValueChanged(object sender, EventArgs e)
        {
            if(dtpSeleccioFechaPartido.Value.DayOfWeek.ToString() == "Tuesday" || dtpSeleccioFechaPartido.Value.DayOfWeek.ToString() == "Thursday")
            {
                this.lbDiaPartido.Text = dtpSeleccioFechaPartido.Value.ToShortDateString();
            }
            else
            {
                MessageBox.Show("Seleccione un día de la semana que sea 'Martes' o 'Jueves'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtpHoraPartido_ValueChanged(object sender, EventArgs e)
        {
            this.lbHoraPartido.Text = dtpHoraPartido.Value.ToShortTimeString();
        }


        //Botón que tedrá el evento de exportarnos todos los datos del partido
        private void btnExportarDatos_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(sfd.FileName);
                if (File.Exists(sfd.FileName))
                    {
                       
                        sw.WriteLine("Fecha: " + lbDiaPartido.Text);
                        sw.WriteLine("Hora: " + lbHoraPartido.Text);
                        sw.WriteLine("Trío Arbitral: " + lbArbitros.Text + "\n");

                        sw.WriteLine("Equipo A: " + lbEquiA.Items.ToString());
                        sw.WriteLine("Equipo B: " + lbEquiB.Items.ToString());

                        sw.WriteLine("\nResultado: 2-0");
                        sw.WriteLine("\nInicidencias: 3");                        
                    }

                sw.Close();
            }
        }




        //----------------------------
        // Métodos 'extras'/ de apoyo
        // Serán llamados desde otras funcionesprincipales,
        // que nos servirán para tener el código un poco más ordenado
        //----------------------------

        //---------------------------------------------------
        // Métodos para rellenar las listas y los comboBox
        //---------------------------------------------------

        //Nos rellenará la lista principal de los jugadores (ListBox lbTodosJugadores)
        private void rellenarListadoJugadores(ListBox jugadoresList)
        {
            //Hacemos un foreach
            foreach (String jugador in this.jugadores)
            {
                jugadoresList.Items.Add(jugador.Trim());
            }

        }

        
        //Nos rellenará los 3combobox de los arbitros y Linner
        private void rellenarComboBoxArbitraje()
        {
            foreach (string arbitro in this.arbitros)
            {
                cbArbitro.Items.Add(arbitro);
                cbLiner1.Items.Add(arbitro);
                cbLiner2.Items.Add(arbitro);
            }

            //Ponemos un título a los comboBox para que visiblemente este mejor
            cbArbitro.Items.Insert(0, "Seleccionar Arbitro");
            cbArbitro.SelectedIndex = 0;

            cbLiner1.Items.Insert(0, "Seleccionar Liner-1");
            cbLiner1.SelectedIndex = 0;

            cbLiner2.Items.Insert(0, "Seleccionar Liner-2");
            cbLiner2.SelectedIndex = 0;
        }


       
        /// <summary>
        /// Nos añadirá varios un jugadores a un listBox, dependiendo del listado que le pasemos
        /// </summary>
        /// <param name="nombreEquipo">Nombre que nos definirá en qué ListBox irá el listado de jugadores</param>
        /// <param name="jugadores">El listado de los jugadores</param>
        /// <param name="opEquipo">Parámetro opcional, se lo pasaremos para que nos borre del listado anterior los jugadores</param>
        private void annadirJugadores(string nombreEquipo, ArrayList jugadores, string opEquipo = "listBox")
        {
            //Listado de jugadores que borraremos próximamente
            ArrayList jugadoresBorrar = new ArrayList();

            //Si añadimos en el equipoA
            if(nombreEquipo == "equipoA")
            {
                //Hacemos un foreach
                foreach (String jugador in jugadores)
                {
                    if (!comprobarJugadorRepetido(jugador , "equipoA"))
                    {
                        if(this.numJugadoresEquipoA < 7)
                        {
                            this.lbEquiA.Items.Add(jugador);
                            jugadoresBorrar.Add(jugador);
                            this.numJugadoresEquipoA += 1;
                        }
                        //Borramos a los jugadores
                        borrarJugadoresListado("listadoPrincipal", jugadoresBorrar);
                    }
                }
            }
            //Si añadimos en el equipoB
            else if (nombreEquipo == "equipoB")
            {
                //Hacemos un foreach
                foreach (String jugador in jugadores)
                {
                    if (!comprobarJugadorRepetido(jugador, "equipoB"))
                    {
                        if (this.numJugadoresEquipoB < 7)
                        {
                            this.lbEquiB.Items.Add(jugador);
                            jugadoresBorrar.Add(jugador);
                            this.numJugadoresEquipoB += 1;
                        }
                        //Borramos a los jugadores
                        borrarJugadoresListado("listadoPrincipal", jugadoresBorrar);
                    }
                }
            //Si añadimos en el listado principal a través de uno de los 2 equipos
            }
            else if (nombreEquipo == "listadoPrincipal")
            {
                foreach (String jugador in jugadores)
                {
                    if (this.numJugadoresEquipoB < 7)
                    {
                        this.lbTodosJugadores.Items.Add(jugador);
                        jugadoresBorrar.Add(jugador);

                        if (opEquipo == "equipoA")
                            this.numJugadoresEquipoA -= 1;

                        if (opEquipo == "equipoB")
                            this.numJugadoresEquipoB -= 1;
                    }
                }
                //Borramos a los jugadores
                borrarJugadoresListado(opEquipo, jugadoresBorrar);
            }
            else
            {
                MessageBox.Show("¡Error inesperado, no se ha reconocido al equipoo en cuestión!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }// Fin annadirJugadores



        //Nos comprueba si el jugador ya está insertado en uno de los equipos
        //  Nos devolverá un 'boolean' (true-está / false-no está)
        //  En el caso de que esté, no nos lo insertará de nuevo
        //  Nos hemos creado este método por si el usuario isertase un jugador ya con ese nombre
        //  y quisierá insertarlo de nuevo en el equipo
        private bool comprobarJugadorRepetido(String jugadorRepetido, String nombreEquipo)
        {
            bool jugRepetido = false;
            if (nombreEquipo == "equipoA")
            {
                for (int i = 0; i < this.lbEquiA.Items.Count && !jugRepetido; i++)
                {
                    if (this.lbEquiA.Items[i] == jugadorRepetido)
                    {
                        jugRepetido = true;
                    }
                }
            }
            else if (nombreEquipo == "equipoB")
            {
                for (int i = 0; i < this.lbEquiB.Items.Count && !jugRepetido; i++)
                {
                    if (this.lbEquiB.Items[i] == jugadorRepetido)
                    {
                        jugRepetido = true;
                    }
                }
            }
            else if (nombreEquipo == "listadoPrincipal")
            {
                for(int i=0; i < this.jugadores.Length && !jugRepetido; i++)
                {
                    if (this.jugadores[i] == jugadorRepetido)
                    {
                        jugRepetido = true;
                    }
                }
            }
            // Control de errores
            else
            {
                MessageBox.Show("¡Error inesperado, no se ha reconocido al equipoo en cuestión!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
            return jugRepetido;
        }// Fin Comprobar jugador Repetido


        
        //Borramos a los jugadores de un determinado equipo, que hemos seleccionado previamente
        //  Nos servirá para borrar al cambiar los jugadores del listBox
        private void borrarJugadoresListado(string equipo_listBox, ArrayList jugadoresList)
        {
            if(equipo_listBox == "equipoA")
            {
                foreach (String jugador in jugadoresList)
                {
                    this.lbEquiA.Items.Remove(jugador);
                }

            }
            else if (equipo_listBox == "equipoB")
            {
                foreach (String jugador in jugadoresList)
                {
                    this.lbEquiB.Items.Remove(jugador);
                }

            }else if (equipo_listBox == "listadoPrincipal")
            {
                foreach (String jugador in jugadoresList)
                {
                    this.lbTodosJugadores.Items.Remove(jugador);
                }
            }
        }// Fin borrar jugadores del listado

        
        //Este método auxiliar nos servirá para poder formatear bien la cadena del
        // label, de donde estarán los nombres de los arbitros
        // En los input nos quitara los espacios y las comas
        // En los output nos añadirá las comas en los arbitros excepto en el último
        private String[] formatearCadena(string cadena, string entrada)
        {
            //Separamos las palabras
            String[] palabrasCadena = cadena.Split(' ');

            //Comprobamos si es input/output, para, dependiendo de un modo u otro
            if (entrada == "input")
            {
                //Quitamos las ',' y los espacios
                for (int i=0; i < palabrasCadena.Length; i++)
                {
                    palabrasCadena[i] = palabrasCadena[i].Replace(',', ' ').Trim();
                }                
            }
            else
            {
                //Añadimos las ',' y los espacios
                for (int i=0; i < palabrasCadena.Length-1; i++)
                {
                    if (palabrasCadena[i] != " ")
                    {
                        palabrasCadena[i] = palabrasCadena[i] + ", ";
                    }                    
                }
            }
            return palabrasCadena;
        }



        //Nos borrará el arbitro que hayamos seleccionado
        private void borrarArbitro(string arbitro)
        {
            cbArbitro.Items.Remove(arbitro);           
            cbLiner1.Items.Remove(arbitro);        
            cbLiner2.Items.Remove(arbitro);           
        }


        //Nos insertará el arbitro que hayamos seleccionado
        private void insertarArbitro(string arbitro)
        {
            cbArbitro.Items.Add(arbitro);
            cbLiner1.Items.Add(arbitro);
            cbLiner2.Items.Add(arbitro);
        }



        //Nos comprueba si ya se ha añadido un arbitro previamente, antes de añadir otro
        // Nos devolverá:
        // true - Si hemos añadido previamente
        // false - Si no hemos añadido ninguno todavía
        private bool arbitroAnnadidoPreviamente(string tipoarbitro)
        {
            
            String[] arbitros = formatearCadena(lbArbitros.Text, "input");
            bool arbitroAnnadido = true;

            if(tipoarbitro == "arbitro")
            {
                for(int i=0; i < arbitros.Length; i++)
                {
                    if (arbitros[i] == "xxx")
                        arbitroAnnadido = false;
                }

            }else if (tipoarbitro == "liner1")
            {
                for (int i = 0; i < arbitros.Length; i++)
                {
                    if (arbitros[i] == "xxxx")
                        arbitroAnnadido = false;
                }

            }
            else if (tipoarbitro == "liner2")
            {
                for (int i = 0; i < arbitros.Length; i++)
                {
                    if (arbitros[i] == "xxxxx")
                        arbitroAnnadido = false;
                }
            }
            return arbitroAnnadido;
        }        
    }
}
