﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lbTodosJugadores = new System.Windows.Forms.ListBox();
            this.lbEquiA = new System.Windows.Forms.ListBox();
            this.lbEquiB = new System.Windows.Forms.ListBox();
            this.btnInsertJugEquipA = new System.Windows.Forms.Button();
            this.btnDevolverTodosJugEquipA = new System.Windows.Forms.Button();
            this.btnDevolverJugEquipA = new System.Windows.Forms.Button();
            this.btnDevolverJugEquipB = new System.Windows.Forms.Button();
            this.btnDevolverTodosJugEquipB = new System.Windows.Forms.Button();
            this.btnInsertJugEquipB = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnEliminarJug = new System.Windows.Forms.Button();
            this.btnAnnadirJugador = new System.Windows.Forms.Button();
            this.tbAnnadirJugador = new System.Windows.Forms.TextBox();
            this.panelArbitraje = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAnnadirLiner2 = new System.Windows.Forms.Button();
            this.btnAnnadirLiner1 = new System.Windows.Forms.Button();
            this.btnAnnadriArbitro = new System.Windows.Forms.Button();
            this.cbLiner2 = new System.Windows.Forms.ComboBox();
            this.cbLiner1 = new System.Windows.Forms.ComboBox();
            this.cbArbitro = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpSeleccioFechaPartido = new System.Windows.Forms.DateTimePicker();
            this.dtpHoraPartido = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbHoraPartido = new System.Windows.Forms.Label();
            this.lbDiaPartido = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbArbitros = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCargarJugad = new System.Windows.Forms.Button();
            this.gbDatos2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExportarDatos = new System.Windows.Forms.Button();
            this.panelArbitraje.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbDatos2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbTodosJugadores
            // 
            this.lbTodosJugadores.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTodosJugadores.FormattingEnabled = true;
            this.lbTodosJugadores.ItemHeight = 18;
            this.lbTodosJugadores.Location = new System.Drawing.Point(13, 52);
            this.lbTodosJugadores.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbTodosJugadores.Name = "lbTodosJugadores";
            this.lbTodosJugadores.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbTodosJugadores.Size = new System.Drawing.Size(300, 418);
            this.lbTodosJugadores.TabIndex = 1;
            // 
            // lbEquiA
            // 
            this.lbEquiA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEquiA.FormattingEnabled = true;
            this.lbEquiA.ItemHeight = 16;
            this.lbEquiA.Location = new System.Drawing.Point(400, 67);
            this.lbEquiA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbEquiA.Name = "lbEquiA";
            this.lbEquiA.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbEquiA.Size = new System.Drawing.Size(228, 116);
            this.lbEquiA.TabIndex = 2;
            // 
            // lbEquiB
            // 
            this.lbEquiB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEquiB.FormattingEnabled = true;
            this.lbEquiB.ItemHeight = 16;
            this.lbEquiB.Location = new System.Drawing.Point(400, 272);
            this.lbEquiB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lbEquiB.Name = "lbEquiB";
            this.lbEquiB.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbEquiB.Size = new System.Drawing.Size(228, 116);
            this.lbEquiB.TabIndex = 3;
            // 
            // btnInsertJugEquipA
            // 
            this.btnInsertJugEquipA.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertJugEquipA.Location = new System.Drawing.Point(330, 67);
            this.btnInsertJugEquipA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnInsertJugEquipA.Name = "btnInsertJugEquipA";
            this.btnInsertJugEquipA.Size = new System.Drawing.Size(54, 37);
            this.btnInsertJugEquipA.TabIndex = 4;
            this.btnInsertJugEquipA.Text = ">";
            this.btnInsertJugEquipA.UseVisualStyleBackColor = true;
            this.btnInsertJugEquipA.Click += new System.EventHandler(this.btnInsertJugEquipA_Click);
            // 
            // btnDevolverTodosJugEquipA
            // 
            this.btnDevolverTodosJugEquipA.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevolverTodosJugEquipA.Location = new System.Drawing.Point(330, 153);
            this.btnDevolverTodosJugEquipA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDevolverTodosJugEquipA.Name = "btnDevolverTodosJugEquipA";
            this.btnDevolverTodosJugEquipA.Size = new System.Drawing.Size(54, 37);
            this.btnDevolverTodosJugEquipA.TabIndex = 5;
            this.btnDevolverTodosJugEquipA.Text = "<<";
            this.btnDevolverTodosJugEquipA.UseVisualStyleBackColor = true;
            this.btnDevolverTodosJugEquipA.Click += new System.EventHandler(this.btnDevolverTodosJugEquipA_Click);
            // 
            // btnDevolverJugEquipA
            // 
            this.btnDevolverJugEquipA.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevolverJugEquipA.Location = new System.Drawing.Point(330, 110);
            this.btnDevolverJugEquipA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDevolverJugEquipA.Name = "btnDevolverJugEquipA";
            this.btnDevolverJugEquipA.Size = new System.Drawing.Size(54, 37);
            this.btnDevolverJugEquipA.TabIndex = 6;
            this.btnDevolverJugEquipA.Text = "<";
            this.btnDevolverJugEquipA.UseVisualStyleBackColor = true;
            this.btnDevolverJugEquipA.Click += new System.EventHandler(this.btnDevolverJugEquipA_Click);
            // 
            // btnDevolverJugEquipB
            // 
            this.btnDevolverJugEquipB.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevolverJugEquipB.Location = new System.Drawing.Point(330, 315);
            this.btnDevolverJugEquipB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDevolverJugEquipB.Name = "btnDevolverJugEquipB";
            this.btnDevolverJugEquipB.Size = new System.Drawing.Size(54, 37);
            this.btnDevolverJugEquipB.TabIndex = 9;
            this.btnDevolverJugEquipB.Text = "<";
            this.btnDevolverJugEquipB.UseVisualStyleBackColor = true;
            this.btnDevolverJugEquipB.Click += new System.EventHandler(this.btnDevolverJugEquipB_Click);
            // 
            // btnDevolverTodosJugEquipB
            // 
            this.btnDevolverTodosJugEquipB.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevolverTodosJugEquipB.Location = new System.Drawing.Point(330, 358);
            this.btnDevolverTodosJugEquipB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDevolverTodosJugEquipB.Name = "btnDevolverTodosJugEquipB";
            this.btnDevolverTodosJugEquipB.Size = new System.Drawing.Size(54, 37);
            this.btnDevolverTodosJugEquipB.TabIndex = 8;
            this.btnDevolverTodosJugEquipB.Text = "<<";
            this.btnDevolverTodosJugEquipB.UseVisualStyleBackColor = true;
            this.btnDevolverTodosJugEquipB.Click += new System.EventHandler(this.btnDevolverTodosJugEquipB_Click);
            // 
            // btnInsertJugEquipB
            // 
            this.btnInsertJugEquipB.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertJugEquipB.Location = new System.Drawing.Point(330, 272);
            this.btnInsertJugEquipB.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnInsertJugEquipB.Name = "btnInsertJugEquipB";
            this.btnInsertJugEquipB.Size = new System.Drawing.Size(54, 37);
            this.btnInsertJugEquipB.TabIndex = 7;
            this.btnInsertJugEquipB.Text = ">";
            this.btnInsertJugEquipB.UseVisualStyleBackColor = true;
            this.btnInsertJugEquipB.Click += new System.EventHandler(this.btnInsertJugEquipB_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.ForestGreen;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(415, 42);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 22);
            this.label2.TabIndex = 10;
            this.label2.Text = "Equipo A";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.ForestGreen;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(409, 244);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 22);
            this.label3.TabIndex = 11;
            this.label3.Text = "Equipo B";
            // 
            // btnEliminarJug
            // 
            this.btnEliminarJug.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarJug.Location = new System.Drawing.Point(13, 523);
            this.btnEliminarJug.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnEliminarJug.Name = "btnEliminarJug";
            this.btnEliminarJug.Size = new System.Drawing.Size(300, 32);
            this.btnEliminarJug.TabIndex = 12;
            this.btnEliminarJug.Text = "ELIMINAR JUGADORES";
            this.btnEliminarJug.UseVisualStyleBackColor = true;
            this.btnEliminarJug.Click += new System.EventHandler(this.btnEliminarJug_Click);
            // 
            // btnAnnadirJugador
            // 
            this.btnAnnadirJugador.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnadirJugador.Location = new System.Drawing.Point(13, 598);
            this.btnAnnadirJugador.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnnadirJugador.Name = "btnAnnadirJugador";
            this.btnAnnadirJugador.Size = new System.Drawing.Size(300, 28);
            this.btnAnnadirJugador.TabIndex = 13;
            this.btnAnnadirJugador.Text = "AÑADIR JUGADOR";
            this.btnAnnadirJugador.UseVisualStyleBackColor = true;
            this.btnAnnadirJugador.Click += new System.EventHandler(this.btnAnnadirJugador_Click);
            // 
            // tbAnnadirJugador
            // 
            this.tbAnnadirJugador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnnadirJugador.Location = new System.Drawing.Point(13, 562);
            this.tbAnnadirJugador.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbAnnadirJugador.Multiline = true;
            this.tbAnnadirJugador.Name = "tbAnnadirJugador";
            this.tbAnnadirJugador.Size = new System.Drawing.Size(300, 30);
            this.tbAnnadirJugador.TabIndex = 14;
            // 
            // panelArbitraje
            // 
            this.panelArbitraje.BackColor = System.Drawing.Color.Transparent;
            this.panelArbitraje.Controls.Add(this.label7);
            this.panelArbitraje.Controls.Add(this.label6);
            this.panelArbitraje.Controls.Add(this.label5);
            this.panelArbitraje.Controls.Add(this.label4);
            this.panelArbitraje.Controls.Add(this.btnAnnadirLiner2);
            this.panelArbitraje.Controls.Add(this.btnAnnadirLiner1);
            this.panelArbitraje.Controls.Add(this.btnAnnadriArbitro);
            this.panelArbitraje.Controls.Add(this.cbLiner2);
            this.panelArbitraje.Controls.Add(this.cbLiner1);
            this.panelArbitraje.Controls.Add(this.cbArbitro);
            this.panelArbitraje.Location = new System.Drawing.Point(22, 188);
            this.panelArbitraje.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelArbitraje.Name = "panelArbitraje";
            this.panelArbitraje.Size = new System.Drawing.Size(351, 167);
            this.panelArbitraje.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.ForestGreen;
            this.label7.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label7.Location = new System.Drawing.Point(67, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(158, 22);
            this.label7.TabIndex = 17;
            this.label7.Text = "Selección Arbitros";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(5, 143);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "Liner 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Location = new System.Drawing.Point(5, 100);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "Liner 1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(5, 53);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 19);
            this.label4.TabIndex = 11;
            this.label4.Text = "Arbitro";
            // 
            // btnAnnadirLiner2
            // 
            this.btnAnnadirLiner2.Enabled = false;
            this.btnAnnadirLiner2.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnadirLiner2.Location = new System.Drawing.Point(299, 134);
            this.btnAnnadirLiner2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnnadirLiner2.Name = "btnAnnadirLiner2";
            this.btnAnnadirLiner2.Size = new System.Drawing.Size(41, 26);
            this.btnAnnadirLiner2.TabIndex = 10;
            this.btnAnnadirLiner2.Text = "+";
            this.btnAnnadirLiner2.UseVisualStyleBackColor = true;
            this.btnAnnadirLiner2.Click += new System.EventHandler(this.btnAnnadirLiner2_Click);
            // 
            // btnAnnadirLiner1
            // 
            this.btnAnnadirLiner1.Enabled = false;
            this.btnAnnadirLiner1.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnadirLiner1.Location = new System.Drawing.Point(299, 91);
            this.btnAnnadirLiner1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnnadirLiner1.Name = "btnAnnadirLiner1";
            this.btnAnnadirLiner1.Size = new System.Drawing.Size(41, 26);
            this.btnAnnadirLiner1.TabIndex = 9;
            this.btnAnnadirLiner1.Text = "+";
            this.btnAnnadirLiner1.UseVisualStyleBackColor = true;
            this.btnAnnadirLiner1.Click += new System.EventHandler(this.btnAnnadirLiner1_Click);
            // 
            // btnAnnadriArbitro
            // 
            this.btnAnnadriArbitro.Enabled = false;
            this.btnAnnadriArbitro.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnadriArbitro.Location = new System.Drawing.Point(299, 44);
            this.btnAnnadriArbitro.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnnadriArbitro.Name = "btnAnnadriArbitro";
            this.btnAnnadriArbitro.Size = new System.Drawing.Size(41, 26);
            this.btnAnnadriArbitro.TabIndex = 8;
            this.btnAnnadriArbitro.Text = "+";
            this.btnAnnadriArbitro.UseVisualStyleBackColor = true;
            this.btnAnnadriArbitro.Click += new System.EventHandler(this.btnAnnadriArbitro_Click);
            // 
            // cbLiner2
            // 
            this.cbLiner2.Enabled = false;
            this.cbLiner2.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLiner2.FormattingEnabled = true;
            this.cbLiner2.Location = new System.Drawing.Point(75, 137);
            this.cbLiner2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbLiner2.Name = "cbLiner2";
            this.cbLiner2.Size = new System.Drawing.Size(216, 25);
            this.cbLiner2.TabIndex = 2;
            this.cbLiner2.SelectedIndexChanged += new System.EventHandler(this.cbLiner2_SelectedIndexChanged);
            // 
            // cbLiner1
            // 
            this.cbLiner1.Enabled = false;
            this.cbLiner1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLiner1.FormattingEnabled = true;
            this.cbLiner1.Location = new System.Drawing.Point(75, 94);
            this.cbLiner1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbLiner1.Name = "cbLiner1";
            this.cbLiner1.Size = new System.Drawing.Size(216, 25);
            this.cbLiner1.TabIndex = 1;
            this.cbLiner1.SelectedIndexChanged += new System.EventHandler(this.cbLiner1_SelectedIndexChanged);
            // 
            // cbArbitro
            // 
            this.cbArbitro.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbArbitro.FormattingEnabled = true;
            this.cbArbitro.Location = new System.Drawing.Point(75, 47);
            this.cbArbitro.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbArbitro.Name = "cbArbitro";
            this.cbArbitro.Size = new System.Drawing.Size(216, 25);
            this.cbArbitro.TabIndex = 0;
            this.cbArbitro.SelectedIndexChanged += new System.EventHandler(this.cbArbitro_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.ForestGreen;
            this.label9.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label9.Location = new System.Drawing.Point(18, 16);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 22);
            this.label9.TabIndex = 19;
            this.label9.Text = "Fecha Partido";
            // 
            // dtpSeleccioFechaPartido
            // 
            this.dtpSeleccioFechaPartido.CalendarFont = new System.Drawing.Font("Yu Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpSeleccioFechaPartido.Font = new System.Drawing.Font("Yu Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpSeleccioFechaPartido.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpSeleccioFechaPartido.Location = new System.Drawing.Point(22, 41);
            this.dtpSeleccioFechaPartido.MinDate = new System.DateTime(2020, 11, 17, 0, 0, 0, 0);
            this.dtpSeleccioFechaPartido.Name = "dtpSeleccioFechaPartido";
            this.dtpSeleccioFechaPartido.Size = new System.Drawing.Size(289, 28);
            this.dtpSeleccioFechaPartido.TabIndex = 20;
            this.dtpSeleccioFechaPartido.ValueChanged += new System.EventHandler(this.dtpSeleccioFechaPartido_ValueChanged);
            // 
            // dtpHoraPartido
            // 
            this.dtpHoraPartido.CalendarFont = new System.Drawing.Font("Yu Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHoraPartido.CustomFormat = "H:mm";
            this.dtpHoraPartido.Font = new System.Drawing.Font("Yu Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHoraPartido.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHoraPartido.Location = new System.Drawing.Point(22, 120);
            this.dtpHoraPartido.MinDate = new System.DateTime(2020, 11, 17, 0, 0, 0, 0);
            this.dtpHoraPartido.Name = "dtpHoraPartido";
            this.dtpHoraPartido.Size = new System.Drawing.Size(289, 28);
            this.dtpHoraPartido.TabIndex = 22;
            this.dtpHoraPartido.Value = new System.DateTime(2020, 11, 17, 10, 0, 0, 0);
            this.dtpHoraPartido.ValueChanged += new System.EventHandler(this.dtpHoraPartido_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label10.Location = new System.Drawing.Point(18, 95);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 22);
            this.label10.TabIndex = 21;
            this.label10.Text = "Hora Partido";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lbHoraPartido);
            this.groupBox1.Controls.Add(this.lbDiaPartido);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lbArbitros);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Font = new System.Drawing.Font("Yu Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Location = new System.Drawing.Point(330, 426);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(695, 154);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Partido";
            // 
            // lbHoraPartido
            // 
            this.lbHoraPartido.AutoSize = true;
            this.lbHoraPartido.BackColor = System.Drawing.Color.Transparent;
            this.lbHoraPartido.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHoraPartido.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbHoraPartido.Location = new System.Drawing.Point(156, 60);
            this.lbHoraPartido.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbHoraPartido.Name = "lbHoraPartido";
            this.lbHoraPartido.Size = new System.Drawing.Size(0, 19);
            this.lbHoraPartido.TabIndex = 21;
            // 
            // lbDiaPartido
            // 
            this.lbDiaPartido.AutoSize = true;
            this.lbDiaPartido.BackColor = System.Drawing.Color.Transparent;
            this.lbDiaPartido.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDiaPartido.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbDiaPartido.Location = new System.Drawing.Point(156, 25);
            this.lbDiaPartido.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDiaPartido.Name = "lbDiaPartido";
            this.lbDiaPartido.Size = new System.Drawing.Size(0, 19);
            this.lbDiaPartido.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label8.Location = new System.Drawing.Point(7, 96);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(102, 19);
            this.label8.TabIndex = 19;
            this.label8.Text = "Trío Arbitral:";
            // 
            // lbArbitros
            // 
            this.lbArbitros.AutoSize = true;
            this.lbArbitros.BackColor = System.Drawing.Color.Transparent;
            this.lbArbitros.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbArbitros.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbArbitros.Location = new System.Drawing.Point(156, 96);
            this.lbArbitros.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbArbitros.Name = "lbArbitros";
            this.lbArbitros.Size = new System.Drawing.Size(133, 19);
            this.lbArbitros.TabIndex = 18;
            this.lbArbitros.Text = "xxx, xxxx, xxxxx";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label12.Location = new System.Drawing.Point(65, 25);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 19);
            this.label12.TabIndex = 13;
            this.label12.Text = "Día:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label11.Location = new System.Drawing.Point(53, 60);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 19);
            this.label11.TabIndex = 12;
            this.label11.Text = "Hora:";
            // 
            // btnCargarJugad
            // 
            this.btnCargarJugad.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargarJugad.ForeColor = System.Drawing.Color.ForestGreen;
            this.btnCargarJugad.Location = new System.Drawing.Point(13, 476);
            this.btnCargarJugad.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnCargarJugad.Name = "btnCargarJugad";
            this.btnCargarJugad.Size = new System.Drawing.Size(300, 32);
            this.btnCargarJugad.TabIndex = 24;
            this.btnCargarJugad.Text = "CARGAR JUGADORES";
            this.btnCargarJugad.UseVisualStyleBackColor = true;
            this.btnCargarJugad.Click += new System.EventHandler(this.btnCargarJugad_Click);
            // 
            // gbDatos2
            // 
            this.gbDatos2.BackColor = System.Drawing.Color.Transparent;
            this.gbDatos2.Controls.Add(this.label9);
            this.gbDatos2.Controls.Add(this.dtpSeleccioFechaPartido);
            this.gbDatos2.Controls.Add(this.label10);
            this.gbDatos2.Controls.Add(this.panelArbitraje);
            this.gbDatos2.Controls.Add(this.dtpHoraPartido);
            this.gbDatos2.Location = new System.Drawing.Point(651, 42);
            this.gbDatos2.Name = "gbDatos2";
            this.gbDatos2.Size = new System.Drawing.Size(374, 378);
            this.gbDatos2.TabIndex = 25;
            this.gbDatos2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.ForestGreen;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(13, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jugadores Disponibles";
            // 
            // btnExportarDatos
            // 
            this.btnExportarDatos.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportarDatos.ForeColor = System.Drawing.Color.ForestGreen;
            this.btnExportarDatos.Location = new System.Drawing.Point(328, 594);
            this.btnExportarDatos.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExportarDatos.Name = "btnExportarDatos";
            this.btnExportarDatos.Size = new System.Drawing.Size(705, 32);
            this.btnExportarDatos.TabIndex = 26;
            this.btnExportarDatos.Text = "EXPORTAR DATOS DEL PARTIDO";
            this.btnExportarDatos.UseVisualStyleBackColor = true;
            this.btnExportarDatos.Click += new System.EventHandler(this.btnExportarDatos_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1079, 647);
            this.Controls.Add(this.btnExportarDatos);
            this.Controls.Add(this.gbDatos2);
            this.Controls.Add(this.btnCargarJugad);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbAnnadirJugador);
            this.Controls.Add(this.btnAnnadirJugador);
            this.Controls.Add(this.btnEliminarJug);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDevolverJugEquipB);
            this.Controls.Add(this.btnDevolverTodosJugEquipB);
            this.Controls.Add(this.btnInsertJugEquipB);
            this.Controls.Add(this.btnDevolverJugEquipA);
            this.Controls.Add(this.btnDevolverTodosJugEquipA);
            this.Controls.Add(this.btnInsertJugEquipA);
            this.Controls.Add(this.lbEquiB);
            this.Controls.Add(this.lbEquiA);
            this.Controls.Add(this.lbTodosJugadores);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fútbol 7";
            this.panelArbitraje.ResumeLayout(false);
            this.panelArbitraje.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbDatos2.ResumeLayout(false);
            this.gbDatos2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lbTodosJugadores;
        private System.Windows.Forms.ListBox lbEquiA;
        private System.Windows.Forms.ListBox lbEquiB;
        private System.Windows.Forms.Button btnInsertJugEquipA;
        private System.Windows.Forms.Button btnDevolverTodosJugEquipA;
        private System.Windows.Forms.Button btnDevolverJugEquipA;
        private System.Windows.Forms.Button btnDevolverJugEquipB;
        private System.Windows.Forms.Button btnDevolverTodosJugEquipB;
        private System.Windows.Forms.Button btnInsertJugEquipB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnEliminarJug;
        private System.Windows.Forms.Button btnAnnadirJugador;
        private System.Windows.Forms.TextBox tbAnnadirJugador;
        private System.Windows.Forms.Panel panelArbitraje;
        private System.Windows.Forms.ComboBox cbLiner2;
        private System.Windows.Forms.ComboBox cbLiner1;
        private System.Windows.Forms.ComboBox cbArbitro;
        private System.Windows.Forms.Button btnAnnadirLiner2;
        private System.Windows.Forms.Button btnAnnadirLiner1;
        private System.Windows.Forms.Button btnAnnadriArbitro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpSeleccioFechaPartido;
        private System.Windows.Forms.DateTimePicker dtpHoraPartido;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbHoraPartido;
        private System.Windows.Forms.Label lbDiaPartido;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbArbitros;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCargarJugad;
        private System.Windows.Forms.GroupBox gbDatos2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExportarDatos;
    }
}

